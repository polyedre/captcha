# encoding: utf-8

import random
import re
from datetime import datetime, timedelta

from flask import g, jsonify, render_template, request
from sqlalchemy.sql.expression import func

import config
from app.controller.controller import Controller
from app.model.audio_challenge import AudioChallengeModel
from app.model.key import KeyModel
from app.model.solution import SolutionModel
from app.model.visual_challenge import VisualChallengeModel
from app.model.visual_source import VisualSourceModel


class Captcha(Controller):
    def librecaptcha(self):
        """
        This method prepares javascript that will call Captcha generation
        """
        g.host_url = config.LIBRECAPTCHA_HOST
        return render_template("captcha/librecaptcha.js")

    def recaptcha(self):
        """
        This method prepares javascript that will call Captcha generation
        """
        g.host_url = config.LIBRECAPTCHA_HOST
        return render_template("captcha/recaptcha.js")

    def generate_challenge(self):
        if request.args.get("k") == config.TEST_PUBLIC_KEY:
            # TEST CHALLENGES
            # Visual challenge
            g.visual_source = VisualSourceModel(
                filename="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e72f.jpg",
                author="LibreCaptcha System",
                license="CC0",
            )
            g.visual_challenges = [
                VisualChallengeModel(
                    token="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e720",
                )
            ]
            for i in range(8):
                g.visual_challenges.append(
                    VisualChallengeModel(
                        token="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e721",
                    )
                )
            # Audio challenge
            g.audio_challenge = AudioChallengeModel(
                token="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e72a",
                solution=".",
                author="LibreCaptcha System",
                license="CC0",
            )
            # Solution
            g.solution_token = (
                "fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e728"
            )
        else:
            site_host = re.search("^https?://([^:/]*).*$", request.referrer).group(1)
            site_key = KeyModel.query.filter_by(public=request.args.get("k")).first()
            if request.args.get("t") == "rc":
                g.classname = "g-recaptcha"
            else:
                g.classname = "librecaptcha"
            if site_key is None:
                # TODO: Return error message
                message = "Public Key not found"
                message += "\nKey       : %s" % request.args.get("k")
                message += "\nSite host : %s" % site_host
                print(message)
                g.error = "Public key '%s' is unknown." % request.args.get("k")
                return render_template("captcha/librecaptcha_error.js")
            elif site_host not in [domain.name for domain in site_key.domains.all()]:
                # TODO: Return error message
                message = "Domain not found"
                message += "\nKey       : %s" % request.args.get("k")
                message += "\nSite host : %s" % site_host
                message += "\nDomains   : %s" % [
                    domain.name for domain in site_key.domains.all()
                ]
                print(message)
                g.error = "Domain '%s' is unknown for this public key." % site_host
                return render_template("captcha/librecaptcha_error.js")
            # Get a random visual challenge and disable it
            visual_challenge = (
                VisualChallengeModel.query.filter_by(inactive=False)
                .order_by(func.random())
                .limit(1)
                .first()
            )
            visual_challenge.inactive = True
            visual_challenge.save()
            g.visual_source = visual_challenge.visual_source
            g.visual_challenges = [visual_challenge]
            for i in range(8):
                g.visual_challenges.append(
                    VisualChallengeModel.query.filter_by(inactive=False)
                    .filter(
                        VisualChallengeModel.visual_source_id
                        != visual_challenge.visual_source_id
                    )
                    .order_by(func.random())
                    .limit(1)
                    .first()
                )
            random.shuffle(g.visual_challenges)
            # Get a random audio challenge and disable it
            g.audio_challenge = (
                AudioChallengeModel.query.filter_by(inactive=False)
                .order_by(func.random())
                .limit(1)
                .first()
            )
            g.audio_challenge.inactive = True
            g.audio_challenge.save()
            # Prepare solution
            solution = SolutionModel(
                public=site_key.public,
                domain=site_host,
                token="%0x" % random.getrandbits(256),
                audio=g.audio_challenge.solution,
                visual=visual_challenge.token,
                date=func.now(),
            )
            solution.save()
            g.solution_token = solution.token
            # Add statistics for this domain
            for domain in site_key.domains.all():
                if site_host == domain.name:
                    domain.displayed = domain.displayed + 1
                    domain.save()

        g.host_url = config.LIBRECAPTCHA_HOST
        # Adapt answer with captcha type if necessary
        if request.args.get("t") == "rc":
            return render_template("captcha/get_challenge_recaptcha.js")
        return render_template("captcha/get_challenge.js")

    def generate_challenge_nojs(self):
        if request.args.get("k") == config.TEST_PUBLIC_KEY:
            # TEST CHALLENGES
            # Visual challenge
            g.visual_source = VisualSourceModel(
                filename="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e72f.jpg",
                author="LibreCaptcha System",
                license="CC0",
            )
            g.visual_challenges = [
                VisualChallengeModel(
                    token="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e720",
                )
            ]
            for i in range(8):
                g.visual_challenges.append(
                    VisualChallengeModel(
                        token="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e721",
                    )
                )
            # Audio challenge
            g.audio_challenge = AudioChallengeModel(
                token="fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e72a",
                solution=".",
                author="LibreCaptcha System",
                license="CC0",
            )
            # Solution
            g.solution_token = (
                "fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e728"
            )
        else:
            site_host = re.search("^https?://([^:/]*).*$", request.referrer).group(1)
            site_key = KeyModel.query.filter_by(public=request.args.get("k")).first()
            if request.args.get("t") == "rc":
                g.classname = "g-recaptcha"
            else:
                g.classname = "librecaptcha"
            if site_key is None:
                # TODO: Return error message
                message = "Public Key not found"
                message += "\nKey       : %s" % request.args.get("k")
                message += "\nSite host : %s" % site_host
                print(message)
                g.error = "Public key '%s' is unknown." % request.args.get("k")
                return render_template("captcha/librecaptcha_error.js")
            elif site_host not in [domain.name for domain in site_key.domains.all()]:
                # TODO: Return error message
                message = "Domain not found"
                message += "\nKey       : %s" % request.args.get("k")
                message += "\nSite host : %s" % site_host
                message += "\nDomains   : %s" % [
                    domain.name for domain in site_key.domains.all()
                ]
                print(message)
                g.error = "Domain '%s' is unknown for this public key." % site_host
                return render_template("captcha/librecaptcha_error.js")
            # Get a random visual challenge and disable it
            visual_challenge = (
                VisualChallengeModel.query.filter_by(inactive=False)
                .order_by(func.random())
                .limit(1)
                .first()
            )
            visual_challenge.inactive = True
            visual_challenge.save()
            g.visual_source = visual_challenge.visual_source
            g.visual_challenges = [visual_challenge]
            for i in range(8):
                g.visual_challenges.append(
                    VisualChallengeModel.query.filter_by(inactive=False)
                    .filter(
                        VisualChallengeModel.visual_source_id
                        != visual_challenge.visual_source_id
                    )
                    .order_by(func.random())
                    .limit(1)
                    .first()
                )
            random.shuffle(g.visual_challenges)
            # Get a random audio challenge and disable it
            g.audio_challenge = (
                AudioChallengeModel.query.filter_by(inactive=False)
                .order_by(func.random())
                .limit(1)
                .first()
            )
            g.audio_challenge.inactive = True
            g.audio_challenge.save()
            # Prepare solution
            solution = SolutionModel(
                public=site_key.public,
                domain=site_host,
                token="%0x" % random.getrandbits(256),
                audio=g.audio_challenge.solution,
                visual=visual_challenge.token,
                date=func.now(),
            )
            solution.save()
            g.solution_token = solution.token
            # Add statistics for this domain
            for domain in site_key.domains.all():
                if site_host == domain.name:
                    domain.displayed = domain.displayed + 1
                    domain.save()

        g.host_url = config.LIBRECAPTCHA_HOST
        # Adapt answer with captcha type if necessary
        if request.args.get("t") == "rc":
            return render_template("captcha/get_challenge_recaptcha.js")
        return render_template("captcha/get_challenge_nojs.html")

    def verify_challenge(self):
        # Verify if solution given is good
        result = {
            "success": False,
        }
        # Verifying which style is used : LibreCaptcha or ReCaptcha ?
        if "private-key" in request.form:
            private_key = "private-key"
            answer_key = "librecaptcha-answer"
        else:
            private_key = "secret"
            answer_key = "response"
        try:
            site_key = KeyModel.query.filter_by(
                secret=request.form.get(private_key, "")
            ).first()
            if request.form.get(private_key) == config.TEST_PRIVATE_KEY:
                # TESTING KEY
                answer = request.form.get(answer_key, None)
                if (
                    len(answer) == 64
                    and answer
                    == "fed1d3bab8f379682675b9f57be8c6606be89476fb6bce6dc2e7c737cba9e720"
                ):
                    result["success"] = True
                elif answer == ".":
                    result["success"] = True
            elif site_key is not None:
                answer = request.form.get(answer_key, None)
                if len(answer) == 64:
                    # Visual answer
                    solution = (
                        SolutionModel.query.filter(
                            # Solution should have been given fast enough
                            SolutionModel.date
                            >= (
                                datetime.now()
                                - timedelta(minutes=config.SOLUTION_DURATION)
                            )
                        )
                        .filter_by(visual=answer)
                        .first()
                    )
                    if solution is not None:
                        result["success"] = True
                else:
                    if len(answer) == 8:
                        # Visual answer from no javascript challenge ?
                        solution = (
                            SolutionModel.query.filter(
                                # Solution should have been given fast enough
                                SolutionModel.date
                                >= (
                                    datetime.now()
                                    - timedelta(minutes=config.SOLUTION_DURATION)
                                )
                            )
                            .filter(SolutionModel.visual.like("%s%%" % answer))
                            .first()
                        )
                        if solution is not None:
                            result["success"] = True
                    if result["success"] == False:
                        # Audio answer ?
                        solution = (
                            SolutionModel.query.filter(
                                # Solution should have been given fast enough
                                SolutionModel.date
                                >= (
                                    datetime.now()
                                    - timedelta(minutes=config.SOLUTION_DURATION)
                                )
                            )
                            .filter_by(audio=answer)
                            .first()
                        )
                        if solution is not None:
                            result["success"] = True
                # Add statistics for this domain
                # TODO : Arrange stats if solution is 8 chars size
                for domain in site_key.domains.all():
                    if solution.domain == domain.name:
                        if len(answer) == 64 and result["success"] is True:
                            domain.resolved_visual = domain.resolved_visual + 1
                        elif result["success"] is True:
                            domain.resolved_audio = domain.resolved_audio + 1
                        else:
                            domain.failed = domain.failed + 1
                        domain.save()
                solution.delete()
        except Exception as e:
            print(str(e))
            result["success"] = False
        return jsonify(result)

    def verify_recaptcha(self):
        return self.verify_challenge()
