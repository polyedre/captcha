# encoding: utf-8

from flask import g, render_template

import config
from app.controller.controller import Controller
from app.model.audio_challenge import AudioChallengeModel
from app.model.visual_challenge import VisualChallengeModel


class Core(Controller):
    def home(self):
        g.host_url = config.LIBRECAPTCHA_HOST
        g.visual_challenges = VisualChallengeModel.query.filter_by(
            inactive=False
        ).count()
        g.audio_challenges = AudioChallengeModel.query.filter_by(inactive=False).count()
        return render_template("core/home.html")

    def about(self):
        return render_template("core/about.html")

    def faq(self):
        return render_template("core/faq.html")

    def why(self):
        return render_template("core/why.html")

    def documentation(self):
        return render_template("core/documentation.html")

    def bot(self):
        return render_template("core/bot.html")
