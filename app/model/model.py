# encoding: utf-8

from app import db


class Model:
    def save(self):
        is_existing = True
        if len(self.__table__.primary_key.columns.keys()) > 1:
            is_existing = False
        else:
            for column_name in self.__table__.primary_key.columns.keys():
                column = self.__getattribute__(column_name)
                if column is None or column == "":
                    is_existing = False
        if not is_existing:
            db.session.add(self)
        db.session.commit()

    def delete(self):
        is_existing = True
        for column_name in self.__table__.primary_key.columns.keys():
            column = self.__getattribute__(column_name)
            if column is None or column == "":
                is_existing = False
        if is_existing:
            db.session.delete(self)
            db.session.commit()
