# encoding: utf-8

import hashlib

from app import db
from app.model.model import Model


def get_user(user_id):
    return User.query.get(user_id)


class UserModel(db.Model, Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(1000))
    password = db.Column(db.String(128))
    email = db.Column(db.String(1000))
    active = db.Column(db.Boolean)
    admin = db.Column(db.Boolean)

    @property
    def hashed_password(self):
        return self.hashed_password

    @hashed_password.setter
    def hashed_password(self, new_password):
        self.password = hashlib.sha512(new_password.encode("utf-8")).hexdigest()

    def check_password(self, password):
        return self.password == hashlib.sha512(password).hexdigest()

    @property
    def is_authenticated(self):
        return self.id is not None

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_admin(self):
        return self.admin

    def get_id(self):
        return str(self.id)
