# encoding: utf-8
"""
This module is used to provide some utilities to captcha commands.
"""

ACCEPTED_LICENSES = (
    "GFDL",
    "CC0",
    "CC BY",
)


def normalize_license(license):
    """
    For now, licenses could be CC0, CC XX, GFDL.
    If license is not known, it will be reused directly.
    """
    result = license
    if "zero" in license or "cc0" in license:
        result = "CC0"
    elif "by" in license:
        result = "CC BY"
        if "nc" in license:
            result += "-NC"
        if "nd" in license:
            result += "-ND"
        if "sa" in license:
            result += "-SA"
        if "1.0" in license:
            result += " 1.0"
        elif "2.0" in license:
            result += " 2.0"
        elif "2.1" in license:
            result += " 2.1"
        elif "2.5" in license:
            result += " 2.5"
        elif "3.0" in license:
            result += " 3.0"
        elif "4.0" in license:
            result += " 4.0"
    elif "gfdl" in license:
        result = "GFDL"
        if "1.1" in license:
            result += " 1.1"
        elif "1.2" in license:
            result += " 1.2"
        elif "1.3" in license:
            result += " 1.3"
    return result
