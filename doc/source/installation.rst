Installation
============

LibreCaptcha est une application web qui n'`est pas faite pour être installée
en auto-hébergement`_.
LibreCaptcha s'appuie sur plusieurs logiciels qui lui permettent de fonctionner :

* `python3`_ (pour son fonctionnement)
* `mysql-server`_ (pour son fonctionnement interne)
* `ffmpeg`_ (pour la génération des challenges audio)


Paquets systèmes indispensables
-------------------------------

Sur Debian, l'installation des paquets utiles se fait via cette commande

    apt-get install ffmpeg default-mysql-server default-libmysqlclient-dev python3-dev build-essential git


Base de données
---------------

Vous devez maintenant créer une base de données. Connectez-vous à celle-ci avec
un utilisateur ayant les droits de création de base :

.. code-block:: sql

    CREATE DATABASE IF NOT EXISTS `LibreCaptcha` DEFAULT CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;
    CREATE USER 'LibreCaptcha'@'localhost' IDENTIFIED BY 'LibreCaptcha';
    GRANT ALL PRIVILEGES ON LibreCaptcha.* TO 'LibreCaptcha'@'localhost';

.. warning::
    Il peut-être intéressant de spécifier un mot de passe un peu plus complexe.


L'application web
-----------------

Ensuite, vous pouvez cloner le dépôt en utilisant git :

.. code-block:: bash

    git clone https://framagit.org/Mindiell/captcha.git

Rendez-vous dans le répertoire et créez un environnement virtuel, que vous
activerez pour y installer les librairies python nécessaires :

.. code-block:: bash

    cd captcha
    python3 -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt

Copiez et éditez le fichier de configuration

.. code-block:: bash

    cp default_config.py config.py
    vi config.py (vous pouvez aussi utiliser emacs, nano, o uce que vous voulez)

Spécifiez l'application Flask

.. code-block:: bash

    export FLASK_APP=server.py

Et générez votre base de données

.. code-block:: bash

    flask db upgrade


Génération de challenges
------------------------

Pour générer les challenges, il faut vous trouver dans le répertoire de
l'application et avoir spécifier l'application Flask.

.. code-block:: bash

    cd my_project
    export FLASK_APP=server.py

Puis générez les challenges

.. code-block:: bash

    flask captcha generate

.. warning::
    Attention, la génération des challenges peut prendre un temps assez long
    suivant votre connexion internet et la puissance de votre machine. De plus,
    des pauses sont faites automatiquement afin de ne pas gêner les sites
    pourvoyeurs d'images et de sons sources.


Lancement du site
-----------------

Pour lancer le site lui-même, il faut vous trouver dans le répertoire de
l'application et avoir spécifier l'application Flask.

.. code-block:: bash

    cd my_project
    export FLASK_APP=server.py

Si vous souhaitez travailler sur l'application, passez l'environnement en mode
développement

.. code-block:: bash

    export FLASK_ENV=development

Puis lancer le site

.. code-block:: bash

    flask run

.. warning::
    Attention, cette commande sert uniquement à lancer le site en local. Pour
    utiliser le site en production il est nécessaire d'utiliser un `serveur WSGI`_


.. _est pas faite pour être installée en auto-hébergement: https://
.. _python3: https://python.org/
.. _mysql-server: https://python.org/
.. _ffmpeg: https://python.org/
.. _serveur WSGI: https://flask.palletsprojects.com/en/1.1.x/deploying/#deployment

