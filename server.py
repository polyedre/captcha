# encoding: utf-8

from flask import current_app, Flask, session
from flask_babel import Babel
from flask_login import LoginManager
from markdown import markdown

from app import api, db, migrate
from app.model.user import get_user
from app.routes import apis, routes

try:
    from command import commands
except ImportError:
    # no command
    commands = ()


app = Flask(__name__, template_folder="app/view")
app.config.from_object("config")
app.config.from_envvar("ENVIRONMENT_CONFIG", silent=True)
if "JINJA_ENV" in app.config:
    app.jinja_env.trim_blocks = app.config["JINJA_ENV"]["TRIM_BLOCKS"]
    app.jinja_env.lstrip_blocks = app.config["JINJA_ENV"]["LSTRIP_BLOCKS"]
app.jinja_env.filters["markdown"] = lambda t: markdown(t)

# Loading routes
for route in routes:
    if len(route) < 3:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=route[2])
# Loading routes for api
for route in apis:
    api.add_resource(route[1], route[0])

# Initialisation of extensions
api.init_app(app)
babel = Babel(app)
db.init_app(app)
login_manager = LoginManager(app)
migrate.init_app(app, db)

# Manage commands
for command in commands:
    app.cli.add_command(command)


# Manage locale
@babel.localeselector
def get_locale():
    """
    Return locale for flask-babel extension.
    """
    return session.get("locale", current_app.config["BABEL_DEFAULT_LOCALE"])


# Manage user
@login_manager.user_loader
def load_user(user_id):
    """
    Load user from its id.
    """
    return get_user(user_id)


if __name__ == "__main__":
    """
    Initialisation of Flask application and its extensions.
    """
    # Running application
    app.run(
        debug=app.config["DEBUG"],
        host=app.config["HOST"],
        port=app.config["PORT"],
    )
